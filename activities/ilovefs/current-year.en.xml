<?xml version="1.0" encoding="UTF-8"?>

<data>
  <version>1</version>
  <module>

    <h2>I ♥ Free Software Day 2021</h2>

    <p>
      For this year's I love Free Software Day let's think about the changes in
      our daily lives we all went through during the last year. In the past year
      Free Software helped us to <strong>stay connected</strong> with our
      family, friends and loved ones! There are many examples of people who
      promoted the use of Free Software, may it be at a university or school
      which is using Ilias, BigBlueButton or Claroline, the colleague who
      suggested Jitsi for the video call instead of proprietary software tools,
      or the use of Free Software during conferences or online workshops.
    </p>

    <p>
      Let us use the I love Free Software day to say thank you, not only to the
      developers but also to those who promoted and enabled us with their
      commitment to use Free Software!
    </p>

    <figure>
      <img
        src="https://pics.fsfe.org/uploads/small/6905f1c863a2fcafd32853f301235f10.jpg"
        alt="GNU and Tux in a Box" />
          <figcaption>Let's celebrate #ilovefs day!</figcaption>
    </figure>

    <p>
      As a new feature for this years I love Free Software Day we also have a
      new Software Freedom Podcast episode, which is dedicated to the I love
      Free Software Day. Together with Greg Kroah-Hartman from Linux kernel,
      Lydia Pintscher from KDE, Florian Effenberger from The Document
      Foundation, the Free Software lawyer Miriam Ballhausen and Polina Malaja
      from centr.org, we talk about the background of and why we need the I love
      Free Software Day.
    </p>


    <h2>Get active for #ilovefs</h2>

    <div class="icon-grid">
      <ul>
        <li>
          <img src="/graphics/icons/ilovefs-use.png" alt="ILoveFS heart with 'use'"/>
          <div>
            <h3>Use the opportunity</h3>
            <p>
              Take the opportunity this wonderful day to thank the people who
              enable you to enjoy software freedom. Make use of our new Software
              Freedom Podcast Episode and our new <a
              href="https://download.fsfe.org/campaigns/ilovefs/share-pics/">share-pics</a>,
              the <a href="/contribute/spreadtheword#ilovefs">stickers and
              balloons</a>, <a
              href="/activities/ilovefs/artwork/artwork.html">artwork</a>, and
              <a href="/order/order.html">merchandise</a> the FSFE provides for
              <em>#ilovefs</em>.
            </p>

          </div>
        </li>

        <li>
          <img src="/graphics/icons/ilovefs-study.png" alt="ILoveFS heart with 'study'"/>
          <div>
            <h3>Think about it</h3>
            <p>
              Think about whose work for or whose promotion of Free Software
              enabled you to stay connected through Free Software. Was it your
              university or school that enabled you to use Free Software for
              classes? Or maybe your colleague who promoted the use of Free
              Software during the times of home office? Think about Free
              Software that enabled you to stay in touch with the people around
              you. For some inspiration have a look at others' <a href="/activities/ilovefs/whylovefs/gallery.html">love statements</a> or read this inspiring <a href="https://www.fsf.org/bulletin/2020/spring/jitsi-spain">story from the FSF</a>.
            </p>
          </div>
        </li>

        <li>
          <img src="/graphics/icons/ilovefs-share.png" alt="ILoveFS heart with 'share'"/>
          <div>
            <h3>Share your love</h3>
            <p>
              That's the fun part! Share your appreciation on social media
              (<em>#ilovefs</em>), in blog posts, pictures and video messages,
              or directly to the Free Software developers and contributors. And
              why not organise an online meet-up with your friends or
              co-workers?
            </p>
          </div>
        </li>

        <li>
          <img src="/graphics/icons/ilovefs-improve.png" alt="ILoveFS heart with 'improve'"/>
          <div>
            <h3>Improve Free Software</h3>
            <p>
              Talking is silver, contributing is gold! Help a Free Software
              project with code, a translation, or by assisting its users. Or if
              you can, please consider a donation to a Free Software
              organisation <a href="https://my.fsfe.org/donate">like the
              FSFE</a>, or to another project.
            </p>
          </div>
        </li>
      </ul>
    </div>

    <figure class="no-border">
      <img
        src="https://pics.fsfe.org/uploads/medium/de18cae737439ece3bd22fe06b95c7e8.png"
        alt="People celebrating IloveFS" />
    </figure>

    <p>
      What is your contribution to this special day and the people who enable
      you to use Free Software? Will you use our stickers and balloons? Or make
      a picture or video with your new ILoveFS shirt? And what about celebrating
      software freedom with your colleagues and friends at a company gathering
      or public event? Whatever you do, show it to the world by using the
      <em>#ilovefs</em> tag. And if you have questions, <a href="/contact/">just
      drop us an email</a>.
  </p>

    <p>
      Happy <strong><span class="text-danger">I love Free
      Software</span></strong> Day everyone! ❤
    </p>

  </module>
</data>
